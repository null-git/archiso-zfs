$zpool = the zpool name
$crypt = device name after decrypting
$nroot = new root path

# cryptsetup
open setup device:
    cryptsetup open /dev/<device> $crypt

# zfs
list all importable zpools:
    zpool import

import zpool with alternative root:
    zpool import -R $nroot $zpool

# mount
don't forget to mount boot device:
    mount /dev/<boot-device> $nroot/boot

# chroot
use arch-chroot for exposing stuff like /dev or resolve.conf to the chroot:
    arch-chroot $nroot

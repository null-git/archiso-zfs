# How to

Install archiso package:
```
pacman -Syu archiso
```

The archzfs key must be included to the pacman keyring of the building machine:
```
pacman-key --add airootfs/root/.archzfs/archzfs.asc
pacman-key --lsign buildbot@archzfs.com
```

Then just build via:
```
mkarchiso -v -w /root/archiso-zfs/work -o /root/archiso-zfs/out /root/archiso-zfs
```
